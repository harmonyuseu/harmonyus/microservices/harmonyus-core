package eu.harmonyus.core.model;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntityBase;
import io.smallrye.mutiny.Uni;
import org.bson.codecs.pojo.annotations.BsonId;

import java.time.LocalDateTime;

@MongoEntity(collection = "parameter")
public class ParameterEntity extends ReactivePanacheMongoEntityBase {

    @BsonId
    private String code;

    private String value;
    private LocalDateTime creation = null;
    private LocalDateTime modification = null;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public LocalDateTime getModification() {
        return modification;
    }

    public void setModification(LocalDateTime modification) {
        this.modification = modification;
    }

    @Override
    public Uni<Void> persist() {
        if (creation == null) {
            creation = LocalDateTime.now();
        }
        return super.persist();
    }

    @Override
    public Uni<Void> persistOrUpdate() {
        if (modification == null) {
            modification = LocalDateTime.now();
        }
        return super.persistOrUpdate();
    }
}
