package eu.harmonyus.core.service;

import eu.harmonyus.core.client.model.*;
import eu.harmonyus.core.client.service.AuthorService;
import eu.harmonyus.core.client.service.CategoryService;
import eu.harmonyus.core.client.service.PostService;
import eu.harmonyus.core.client.service.TagService;
import eu.harmonyus.core.infrastructure.ApplicationConfiguration;
import eu.harmonyus.core.repository.ParameterRepository;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logmanager.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

@ApplicationScoped
public class InitializeDatabaseService {

    public enum Version {v0_2_0}

    @Inject
    ParameterRepository parameterRepository;

    @Inject
    ApplicationConfiguration applicationConfiguration;

    @Inject
    Logger logger;

    @Inject
    @RestClient
    AuthorService authorService;

    @Inject
    @RestClient
    CategoryService categoryService;

    @Inject
    @RestClient
    PostService postService;

    @Inject
    @RestClient
    TagService tagService;

    @ConfigProperty(name = "harmonyus.microservice.author/mp-rest/url")
    String authorUrl;
    @ConfigProperty(name = "harmonyus.microservice.category/mp-rest/url")
    String categoryUrl;
    @ConfigProperty(name = "harmonyus.microservice.tag/mp-rest/url")
    String tagUrl;

    public void initialize() {
        logger.warning("Create author");
        createAuthor();
        logger.warning("Create category");
        createCategory();
        logger.warning("Create tags");
        createTags();
        logger.warning("Create post");
        createPost();
        logger.warning("Set default title ");
        parameterRepository.setInformationTitle(applicationConfiguration.getApplicationParameters().getTitle().orElse("Harmonyus"));
        logger.warning("Set database version");
        parameterRepository.setSystemDatabaseVersion("0.1.0");
    }

    private void createAuthor() {
        ApplicationConfiguration.AuthorConfig authorConfig = applicationConfiguration.getAuthor();

        authorConfig.getFirstname().ifPresent(firstname -> {
            for (AtomicInteger n = new AtomicInteger(0); n.get() < firstname.size(); n.incrementAndGet()) {
                Author author = new Author();
                author.firstname = firstname.get(n.get());
                authorConfig.getLastname().ifPresent(lastname -> author.lastname = lastname.get(n.get()));
                authorConfig.getEmail().ifPresent(email -> author.email = email.get(n.get()));
                authorConfig.getDescription().ifPresent(description -> author.description = description.get(n.get()));
                authorService.create(author).await().indefinitely();
            }
        });
    }

    private void createCategory() {
        ApplicationConfiguration.CodeNameConfig codeNameConfig = applicationConfiguration.getCategory();

        codeNameConfig.getName().ifPresent(name -> {
            for (AtomicInteger n = new AtomicInteger(0); n.get() < name.size(); n.incrementAndGet()) {
                Category category = new Category();
                category.name = name.get(n.get());
                codeNameConfig.getCode().ifPresent(code -> category.code = code.get(n.get()));
                categoryService.create(category).await().indefinitely();
            }
        });
    }

    private void createTags() {
        ApplicationConfiguration.CodeNameConfig codeNameConfig = applicationConfiguration.getTag();

        codeNameConfig.getName().ifPresent(name -> {
            for (AtomicInteger n = new AtomicInteger(0); n.get() < name.size(); n.incrementAndGet()) {
                Tag tag = new Tag();
                tag.name = name.get(n.get());
                codeNameConfig.getCode().ifPresent(code -> tag.code = code.get(n.get()));
                tagService.create(tag).await().indefinitely();
            }
        });
    }

    private void createPost() {
        ApplicationConfiguration.PostConfig postConfig = applicationConfiguration.getPost();

        postConfig.getTitle().ifPresent(title -> {
            for (AtomicInteger n = new AtomicInteger(0); n.get() < title.size(); n.incrementAndGet()) {
                Post post = new Post();
                post.title = title.get(n.get());
                postConfig.getBody().ifPresent(body -> post.body = loadFile(body.get(n.get())));
                postConfig.getAuthor().ifPresent(author -> post.author = new Post.UrlCode(authorUrl + "/" + authorService.getByCode(author.get(n.get())).await().indefinitely().code));
                postConfig.getCategory().ifPresent(category -> post.category = new Post.UrlCode(categoryUrl + "/" + categoryService.getByCode(category.get(n.get())).await().indefinitely().code));
                postConfig.getTags().ifPresent(tags -> {
                    post.tags = new ArrayList<>();
                    Arrays.stream(tags.get(n.get()).split("\\|")).forEach(code -> post.tags.add(new Post.UrlCode(tagUrl + "/" + tagService.getByCode(code).await().indefinitely().code)));
                });
                postService.create(post).await().indefinitely();
            }
        });
    }

    public void patch(Version version) {

    }

    private String loadFile(String fileName) {
        try {
            return Files.readString(Paths.get(fileName));
        } catch (IOException e) {
            logger.severe("fileName '" + Paths.get(fileName).toAbsolutePath() + "' not found");
            return null;
        }
    }
}
