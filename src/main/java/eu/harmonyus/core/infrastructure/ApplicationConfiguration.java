package eu.harmonyus.core.infrastructure;

import io.quarkus.arc.config.ConfigProperties;

import java.util.List;
import java.util.Optional;

@ConfigProperties(prefix = "harmonyus")
public class ApplicationConfiguration {
    private AuthorConfig author;
    private CodeNameConfig category;
    private CodeNameConfig tag;
    private PostConfig post;
    private ApplicationParametersConfig applicationParameters;

    public AuthorConfig getAuthor() {
        return author;
    }

    public void setAuthor(AuthorConfig author) {
        this.author = author;
    }

    public CodeNameConfig getCategory() {
        return category;
    }

    public void setCategory(CodeNameConfig category) {
        this.category = category;
    }

    public CodeNameConfig getTag() {
        return tag;
    }

    public void setTag(CodeNameConfig tag) {
        this.tag = tag;
    }

    public PostConfig getPost() {
        return post;
    }

    public void setPost(PostConfig post) {
        this.post = post;
    }

    public ApplicationParametersConfig getApplicationParameters() {
        return applicationParameters;
    }

    public void setApplicationParameters(ApplicationParametersConfig applicationParameters) {
        this.applicationParameters = applicationParameters;
    }

    public static class AuthorConfig {
        private Optional<List<String>> firstname;
        private Optional<List<String>> lastname;
        private Optional<List<String>> email;
        private Optional<List<String>> description;

        public Optional<List<String>> getFirstname() {
            return firstname;
        }

        public void setFirstname(Optional<List<String>> firstname) {
            this.firstname = firstname;
        }

        public Optional<List<String>> getLastname() {
            return lastname;
        }

        public void setLastname(Optional<List<String>> lastname) {
            this.lastname = lastname;
        }

        public Optional<List<String>> getEmail() {
            return email;
        }

        public void setEmail(Optional<List<String>> email) {
            this.email = email;
        }

        public Optional<List<String>> getDescription() {
            return description;
        }

        public void setDescription(Optional<List<String>> description) {
            this.description = description;
        }
    }

    public static class CodeNameConfig {
        private Optional<List<String>> name;
        private Optional<List<String>> code;

        public Optional<List<String>> getName() {
            return name;
        }

        public void setName(Optional<List<String>> name) {
            this.name = name;
        }

        public Optional<List<String>> getCode() {
            return code;
        }

        public void setCode(Optional<List<String>> code) {
            this.code = code;
        }
    }

    public static class PostConfig {
        private Optional<List<String>> title;
        private Optional<List<String>> body;
        private Optional<List<String>> author;
        private Optional<List<String>> category;
        private Optional<List<String>> tags;
        private Optional<List<String>> code;

        public Optional<List<String>> getTitle() {
            return title;
        }

        public void setTitle(Optional<List<String>> title) {
            this.title = title;
        }

        public Optional<List<String>> getBody() {
            return body;
        }

        public void setBody(Optional<List<String>> body) {
            this.body = body;
        }

        public Optional<List<String>> getCode() {
            return code;
        }

        public void setCode(Optional<List<String>> code) {
            this.code = code;
        }

        public Optional<List<String>> getAuthor() {
            return author;
        }

        public void setAuthor(Optional<List<String>> author) {
            this.author = author;
        }

        public Optional<List<String>> getCategory() {
            return category;
        }

        public void setCategory(Optional<List<String>> category) {
            this.category = category;
        }

        public Optional<List<String>> getTags() {
            return tags;
        }

        public void setTags(Optional<List<String>> tags) {
            this.tags = tags;
        }
    }

    public static class ApplicationParametersConfig {
        private Optional<String> title;

        public Optional<String> getTitle() {
            return title;
        }

        public void setTitle(Optional<String> title) {
            this.title = title;
        }
    }
}
