package eu.harmonyus.core.client.model;

import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.json.bind.annotation.JsonbTransient;
import java.util.List;

@RegisterForReflection
public class Post {
    public String title;
    public String body;
    public String code;
    public UrlCode author;
    public UrlCode category;
    public List<UrlCode> tags;

    @RegisterForReflection
    public static class UrlCode {
        public String url;

        public UrlCode() {

        }

        public UrlCode(String url) {
            this.url = url;
        }

        @JsonbTransient
        public String getCode() {
            return url.substring(url.lastIndexOf('/') + 1);
        }
    }
}
