package eu.harmonyus.core.client.model;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class Category {
    public String name;
    public String code;
}
