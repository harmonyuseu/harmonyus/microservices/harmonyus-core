package eu.harmonyus.core.repository;

import eu.harmonyus.core.model.ParameterEntity;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoRepositoryBase;
import io.smallrye.mutiny.Uni;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ParameterRepository implements ReactivePanacheMongoRepositoryBase<ParameterEntity, String> {

    private static final String INFORMATION_TITLE = "information.title";
    private static final String SYSTEM_DATABASE_VERSION = "system.database.version";

    public String getInformationTitle() {
        return getParameterValueByCode(INFORMATION_TITLE);
    }

    public void setInformationTitle(String informationTitle) {
        setParameterValueByCode(INFORMATION_TITLE, informationTitle);
    }

    public String getSystemDatabaseVersion() {
        return getParameterValueByCode(SYSTEM_DATABASE_VERSION);
    }

    public void setSystemDatabaseVersion(String systemDatabaseVersion) {
        setParameterValueByCode(SYSTEM_DATABASE_VERSION, systemDatabaseVersion);
    }

    private String getParameterValueByCode(String code) {
        Uni<ParameterEntity> parameter = findById(code);
        return parameter.onItem().ifNull().continueWith(new ParameterEntity()).await().indefinitely().getValue();
    }

    private void setParameterValueByCode(String code, String value) {
        Uni<ParameterEntity> uniParameter = findById(code);
        uniParameter.onItem().ifNotNull().invoke(parameterEntity -> {
            parameterEntity.setValue(value);
            parameterEntity.persistOrUpdate().await().indefinitely();
        }).onItem().ifNull().continueWith(() -> {
            ParameterEntity parameterEntity = new ParameterEntity();
            parameterEntity.setCode(code);
            parameterEntity.setValue(value);
            parameterEntity.persist().await().indefinitely();
            return parameterEntity;
        }).await().indefinitely();
    }
}
