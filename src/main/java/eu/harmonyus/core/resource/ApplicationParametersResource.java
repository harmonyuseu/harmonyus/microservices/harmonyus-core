package eu.harmonyus.core.resource;

import eu.harmonyus.core.repository.ParameterRepository;
import eu.harmonyus.core.resource.model.ApplicationParameters;
import eu.harmonyus.core.service.InitializeDatabaseService;
import io.smallrye.mutiny.Uni;
import org.jboss.logmanager.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/application-parameters")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ApplicationParametersResource {

    @Inject
    ParameterRepository parameterRepository;

    @Inject
    InitializeDatabaseService initializeDatabaseService;

    @Inject
    Logger logger;

    @GET
    public Uni<ApplicationParameters> getSystemInformation() {
        ApplicationParameters information = new ApplicationParameters();
        information.title = parameterRepository.getInformationTitle();
        if (information.title == null && parameterRepository.getSystemDatabaseVersion() == null) {
            logger.info("Initialize database");
            initializeDatabaseService.initialize();
            information.title = parameterRepository.getInformationTitle();
        } else if ("0.1.0".equals(parameterRepository.getSystemDatabaseVersion())) {
            initializeDatabaseService.patch(InitializeDatabaseService.Version.v0_2_0);
        }
        information.version = parameterRepository.getSystemDatabaseVersion();
        return Uni.createFrom().item(information);
    }
}
