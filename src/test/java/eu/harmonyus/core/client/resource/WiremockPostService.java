package eu.harmonyus.core.client.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;
import java.util.Scanner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockPostService implements QuarkusTestResourceLifecycleManager {

    private WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock wireMock = new WireMock(wireMockServer.port());
        addMockPost(wireMock, getPostCreateDatabase());
        addMockPost(wireMock,getPostConnexionSSH());
        addMockPost(wireMock, getPostDisableRootLoginPassword());
        return Collections.singletonMap("harmonyus.microservice.post/mp-rest/url", wireMockServer.baseUrl());
    }

    private void addMockPost(WireMock wireMock, JSONObject post) {
        wireMock.register(post(urlEqualTo("/posts"))
                .withRequestBody(equalToJson(post.toString()))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")));
    }

    private JSONObject getPostCreateDatabase() {
        JSONObject obj = new JSONObject();
        obj.put("author", WiremockAuthorService.getAuthor(true));
        obj.put("body", getBody("posts/mysql/create_database.md"));
        obj.put("title", "MySQL création de la base de données en utilisant le charset UTF-8");
        obj.put("category", WiremockCategoryService.getCategory("mysql", "mysql"));
        JSONArray objTags = new JSONArray();
        addTag(objTags, "create-database", "create database");
        addTag(objTags, "create-user", "create user");
        addTag(objTags, "grant", "grant");
        addTag(objTags, "privileges", "privileges");
        addTag(objTags, "sql", "sql");
        addTag(objTags, "utf8", "utf8");
        obj.put("tags", objTags);
        return obj;
    }

    private JSONObject getPostConnexionSSH() {
        JSONObject obj = new JSONObject();
        obj.put("author", WiremockAuthorService.getAuthor(true));
        obj.put("body", getBody("posts/ubuntu/connexion_ssh.md"));
        obj.put("title", "Connexion par clé SSH sur un serveur Linux");
        obj.put("category", WiremockCategoryService.getCategory("ubuntu", "ubuntu"));
        JSONArray objTags = new JSONArray();
        addTag(objTags, "authentication", "authentication");
        addTag(objTags, "authorized_keys2", "authorized_keys2");
        addTag(objTags, "putty", "putty");
        addTag(objTags, "security", "security");
        addTag(objTags, "ssh", "ssh");
        addTag(objTags, "sshd_config", "sshd_config");
        obj.put("tags", objTags);
        return obj;
    }

    private JSONObject getPostDisableRootLoginPassword() {
        JSONObject obj = new JSONObject();
        obj.put("author", WiremockAuthorService.getAuthor(true));
        obj.put("body", getBody("posts/ubuntu/disable_root_login_password.md"));
        obj.put("title", "Désactivation du login par mot de passe sur un serveur linux");
        obj.put("category", WiremockCategoryService.getCategory("ubuntu", "ubuntu"));
        JSONArray objTags = new JSONArray();
        addTag(objTags, "authentication", "authentication");
        addTag(objTags, "security", "security");
        addTag(objTags, "ssh", "ssh");
        addTag(objTags, "sshd_config", "sshd_config");
        obj.put("tags", objTags);
        return obj;
    }

    private Object getBody(String resourceName) {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(resourceName);
        if (stream != null) {
            return new Scanner(stream, StandardCharsets.UTF_8).useDelimiter("\\A").next();
        }
        return null;
    }

    private void addTag(JSONArray objTags, String code, String name) {
        JSONObject obj = new JSONObject();
        obj.put("code", code);
        obj.put("name", name);
        objTags.appendElement(obj);
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }
}
