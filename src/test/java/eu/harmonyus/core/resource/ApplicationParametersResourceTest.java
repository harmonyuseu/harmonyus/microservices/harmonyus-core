package eu.harmonyus.core.resource;

import eu.harmonyus.core.client.resource.WiremockAuthorService;
import eu.harmonyus.core.client.resource.WiremockCategoryService;
import eu.harmonyus.core.client.resource.WiremockPostService;
import eu.harmonyus.core.client.resource.WiremockTagService;
import eu.harmonyus.core.model.ParameterEntity;
import eu.harmonyus.core.repository.ParameterRepository;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

@QuarkusTest
@QuarkusTestResource(WiremockAuthorService.class)
@QuarkusTestResource(WiremockCategoryService.class)
@QuarkusTestResource(WiremockPostService.class)
@QuarkusTestResource(WiremockTagService.class)
class ApplicationParametersResourceTest {

    @ConfigProperty(name = "quarkus.resteasy.path")
    String restPath;

    @Inject
    ParameterRepository parameterRepository;

    @BeforeEach
    void setup() {
        ParameterEntity.deleteAll().await().indefinitely();
    }

    @Test
    @DisplayName("Test - When Calling GET - /api/{version}/application-parameters should return resource - 200")
    void testGetApplicationParameter() {
        given()
                .when()
                .get(restPath + "/application-parameters")
                .then()
                .statusCode(200)
                .body("title", is("AtIcdNet"));
        assertThat(parameterRepository.getInformationTitle(), notNullValue());
        assertThat(parameterRepository.getSystemDatabaseVersion(), notNullValue());
    }
}
