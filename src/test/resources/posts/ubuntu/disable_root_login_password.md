Ce tutoriel nécessite la mise en place au préalable d’une connexion par clé SSH tel que décrit dans le tutoriel [Connexion par clé SSH sur un serveur Linux](https://www.aticdnet.com/ubuntu/connexion-par-cle-ssh-sur-un-serveur-linux.html)

Cette technique permet de vous mettre à l’abrit des tentatives de login par brute de force.

**ATTENTION :** Après avoir appliqué les insctructions ci-dessous, vous ne serez plus en mesure de vous connecter en utilisant le login et le mot de passe de votre utilisateur.

Si vous perdez votre clé SSH, vous ne serez donc plus en mesure de vous connecter.

**ATTENTION :** En cas d’erreur, vous ne pourrait plus vous connectez sur votre serveur !!

Les commandes ci-dessous doivent être exécutées depuis l’utilisateur root

## Précaution

Ouvrer un terminal SSH de secours en cas d’erreur pour pouvoir revenir en arrière sur la mise en place de ce tutoriel.

Depuis un autre terminal SSH

## Paramétrage du daemon SSH

Editer le fichier sshd_config.

```bash
vi /etc/ssh/sshd_config
				
```

Les clés ci-dessous doivent être mises à « no »

*ChallengeResponseAuthentication no*

*PasswordAuthentication no*

*UsePAM no*

Recharger la configuration

```bash
service ssh reload
				
```

## Validation

Assurez-vous de toujours pouvoir vous connecter en SSH en ouvrant un nouveau terminal SSH.
